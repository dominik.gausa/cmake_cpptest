mkdir -p dl
cd dl


if ! [ -f gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2 ]
then
  wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2018q4/gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2
fi

if ! [ -f gnu-mcu-eclipse-qemu-2.8.0-3-20180523-0703-centos64.tgz ]
then
  wget https://github.com/gnu-mcu-eclipse/qemu/releases/download/v2.8.0-3-20180523/gnu-mcu-eclipse-qemu-2.8.0-3-20180523-0703-centos64.tgz
fi

if ! [ -f cmake-3.13.2-Linux-x86_64.tar.gz ]
then
  wget https://github.com/Kitware/CMake/releases/download/v3.13.2/cmake-3.13.2-Linux-x86_64.tar.gz
fi

if ! [ -d cmake-3.13.2-Linux-x86_64 ]
then
  tar -xf cmake-3.13.2-Linux-x86_64.tar.gz
fi

if ! [ -d "gcc-arm-none-eabi-8-2018-q4-major" ]
then
  echo "Extract Gcc"
  tar -xf gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2
fi

if ! [ -d "gnu-mcu-eclipse" ]
then
  echo "Extract qemu"
  tar -xf gnu-mcu-eclipse-qemu-2.8.0-3-20180523-0703-centos64.tgz
fi

cd ..

docker build -t gcc_cmake_gtest .
