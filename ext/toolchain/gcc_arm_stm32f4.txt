include (${CMAKE_CURRENT_LIST_DIR}/../../CMakeLists.txt.env)

set(CMAKE_SYSTEM_NAME Generic) # Or name of your OS if you have one
set(CMAKE_SYSTEM_PROCESSOR arm) # Or whatever
set(CMAKE_CROSSCOMPILING 1)
cmake_minimum_required (VERSION 3.13)


set(TARGET_ARCH   ARM)
set(TARGET_MACH   ARM_CORTEX_M4)


add_compile_options(-mlittle-endian)
add_compile_options(-mcpu=cortex-m4)
add_compile_options(-mthumb)
add_compile_options(-mfloat-abi=hard)
add_compile_options(-mfpu=fpv4-sp-d16)

add_link_options(-mlittle-endian)
add_link_options(-mcpu=cortex-m4)
add_link_options(-mthumb)
add_link_options(-mfloat-abi=hard)
add_link_options(-mfpu=fpv4-sp-d16)
add_link_options(--specs=nosys.specs)
#add_link_options( -specs=nano.specs -specs=rdimon.specs -lc -lrdimon )
add_link_options( -Wl,-Map=output.map -Wl,-gc-sections )
add_compile_options()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DSTM32F407xx=1 -D__FPU_PRESENT=1")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DSTM32F407xx=1 -D__FPU_PRESENT=1")

add_link_options(-T${CMAKE_CURRENT_LIST_DIR}/../linkerfiles/stm32f407.ld -D__FPU_PRESENT=1)



if(MSVC OR CMAKE_HOST_WIN32)
  set(EXE_EXT ".exe")
endif()

set(CMAKE_ASM_COMPILER  "${tools_GCC}arm-none-eabi-as${EXE_EXT}")
set(CMAKE_C_COMPILER    "${tools_GCC}arm-none-eabi-gcc${EXE_EXT}")
set(CMAKE_CXX_COMPILER  "${tools_GCC}arm-none-eabi-g++${EXE_EXT}")

set(CMAKE_C_LINKER      "${tools_GCC}arm-none-eabi-gcc${EXE_EXT}")
set(CMAKE_CXX_LINKER    "${tools_GCC}arm-none-eabi-g++${EXE_EXT}")


