#pragma once

#include <Util/EmitterReceiver.h>
#include <Util/Executable.h>

class BlinkyExec : public ::Platform::Util::Executable
{
  public:
    ::Platform::Util::DataEmitter<bool>   outputState;

    BlinkyExec():
      m_state(false)
    {}

  protected:
    virtual void OnExecute(){
      outputState.Emit(m_state);
      m_state = !m_state;
    }
  
  private:
    bool m_state;
};
