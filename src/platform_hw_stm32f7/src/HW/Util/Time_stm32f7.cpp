#include <Util/Time.h>
#include "Time_stm32f7.h"
#include <stm32f7xx.h>

namespace Platform 
{
  namespace Util
  {
    void Time_STM32F7::OnStart(float freq){
      SysTick_Config(static_cast<unsigned int>(SystemCoreClock/freq));
    }
  }
}

extern "C" void SysTick_Handler(){
  Platform::Util::Time::Instance()->Tick();
}
