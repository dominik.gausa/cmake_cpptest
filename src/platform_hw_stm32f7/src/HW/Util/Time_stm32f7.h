#include <Util/Time.h>

namespace Platform 
{
  namespace Util
  {
    class Time_STM32F7 : public TimeBase
    {
      protected:
        virtual void OnStart(float freq);
    };
  }
}