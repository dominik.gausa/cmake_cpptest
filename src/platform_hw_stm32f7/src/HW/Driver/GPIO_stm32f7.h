#pragma once

#include <Driver/GPIO.h>



namespace Platform {
  namespace Driver {
    class GPIO_STM32F7 : public GPIO {
      
      public:

        GPIO_STM32F7(const char * const name, Pin& pin) :
          GPIO(name, pin)
        {
          PortEnable(m_pin.port);
        }

        virtual bool SetMode(Mode mode);

        virtual bool Value();

        virtual void Set(const bool state);

      private:
        virtual void PortEnable(unsigned int id);
    };
  };
};