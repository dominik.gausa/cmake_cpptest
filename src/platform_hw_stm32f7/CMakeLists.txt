############################################
## lib platform STM32F7

if(NOT DEFINED TARGET_ARCH)
    error()
endif()
if("${TARGET_ARCH}" STREQUAL "ARM"
        AND "${TARGET_MACH}" STREQUAL "ARM_CORTEX_M7")

    set(libname platform_hw_stm32f7)

    set(DEFINES_${libname}
    VERSION=0x1230)

    file(GLOB_RECURSE SOURCES_${libname}_ASM "${CMAKE_CURRENT_LIST_DIR}/src/*.s")
    file(GLOB_RECURSE SOURCES_${libname}_H "${CMAKE_CURRENT_LIST_DIR}/src/*.h")
    file(GLOB_RECURSE SOURCES_${libname}_C "${CMAKE_CURRENT_LIST_DIR}/src/*.c")
    file(GLOB_RECURSE SOURCES_${libname}_CXX "${CMAKE_CURRENT_LIST_DIR}/src/*.cpp")


    set(SOURCES_${libname}
        ${SOURCES_${libname}_ASM}
        ${SOURCES_${libname}_H}
        ${SOURCES_${libname}_C}
        ${SOURCES_${libname}_CXX})

    source_group(
        TREE ${CMAKE_CURRENT_LIST_DIR} 
        FILES ${SOURCES_${libname}}
        )

    project(${libname})
    add_library(${libname} STATIC ${SOURCES_${libname}})

    target_link_libraries (${libname} platform CMSIS)

    target_include_directories(${libname} PUBLIC "${CMAKE_CURRENT_LIST_DIR}/src")
    set_target_properties(${libname} PROPERTIES COMPILE_DEFINITIONS ${DEFINES_${libname}})

endif()
