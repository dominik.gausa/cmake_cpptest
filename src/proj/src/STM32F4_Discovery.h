#pragma once

#include <HW/Driver/GPIO_stm32f4.h>
#include <HW/Util/Time_stm32f4.h>

class HardwareSTM32F4Discovery
{
  typedef ::Platform::Driver::GPIO_STM32F4 GPIO;
  public:
    GPIO* ledGreen;
    GPIO* ledOrange;
    GPIO* ledRed;
    GPIO* ledBlue;
    GPIO* btnUser;
    
    GPIO::Pin ledGreenPin;
    GPIO::Pin ledOrangePin;
    GPIO::Pin ledRedPin;
    GPIO::Pin ledBluePin;
    GPIO::Pin btnUserPin;
    
    ::Platform::Util::Time_STM32F4 time;

    HardwareSTM32F4Discovery():
      ledGreenPin({3, 12}),
      ledOrangePin({3, 13}),
      ledRedPin({3, 14}),
      ledBluePin({3, 15}),
      btnUserPin({0, 0})
    {

      ledGreen  = new GPIO("ledGreen",  ledGreenPin);
      ledOrange = new GPIO("ledOrange", ledOrangePin);
      ledRed    = new GPIO("ledRed",    ledRedPin);
      ledBlue   = new GPIO("ledBlue",   ledBluePin);
      btnUser   = new GPIO("btnUserPin",  btnUserPin);
      
      ledGreen->SetMode       (GPIO::Mode::OUTPUT);
      ledOrange->SetMode      (GPIO::Mode::OUTPUT);
      ledRed->SetMode         (GPIO::Mode::OUTPUT);
      ledBlue->SetMode        (GPIO::Mode::OUTPUT);
      btnUser->SetMode        (GPIO::Mode::INPUT);
    }
};
