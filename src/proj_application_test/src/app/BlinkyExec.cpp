
#include <gmock/gmock.h>
#include <app/BlinkyExec.h>

TEST(BlinkyExec, Base) {
	// This test is named "Negative", and belongs to the "FactorialTest"
	// test case.

  BlinkyExec blinkyExec;
  ::Platform::Util::DataReceiverStore<bool> rec(false);
  blinkyExec.outputState.Link(&rec);


  blinkyExec.Execute();
	EXPECT_EQ(*rec, false);

  blinkyExec.Execute();
	EXPECT_EQ(*rec, true);

  blinkyExec.Execute();
	EXPECT_EQ(*rec, false);
}
