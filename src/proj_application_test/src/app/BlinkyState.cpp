
#include <gmock/gmock.h>
#include <Util/Time.h>
#include <app/BlinkyState.h>

class TimeMock : public ::Platform::Util::TimeBase
{
  public:
    void SetTime(float time){
      m_ticks = time * m_tickFreq;
    }
    
    TimeMock(float increments = 0.01f):
      m_timeIncrement(increments)
    {}

  protected:   
    virtual void OnStart(float freq){}

    virtual void OnTick(){
      m_ticks += m_tickFreq * m_timeIncrement;
    }
    
    float m_timeIncrement;
};

TEST(BlinkyState, Base) {
  TimeMock time;
  BlinkyState blinkyState;

  ::Platform::Util::DataReceiverStore<bool> rec(false);
  ::Platform::Util::DataEmitter<bool> input;

  time.Start(1000);

  input.Link(&blinkyState.shared.trigger);
  blinkyState.shared.output.Link(&rec);

  input.Emit(false);
  blinkyState.Execute();
  EXPECT_EQ(blinkyState.GetCurrentState(), blinkyState.shared.inactive);
	EXPECT_EQ(*rec, false);

  time.Tick();

  input.Emit(true);
  blinkyState.Execute();
  EXPECT_EQ(blinkyState.GetCurrentState(), blinkyState.shared.inactive);
	EXPECT_EQ(*rec, false);

  for(int i = 0; i <= 100; i++){
    time.Tick();

    input.Emit(false);
    blinkyState.Execute();
    EXPECT_EQ(blinkyState.GetCurrentState(), blinkyState.shared.active);
    EXPECT_EQ(*rec, true);
  }

  time.Tick();

  input.Emit(false);
  blinkyState.Execute();
  EXPECT_EQ(blinkyState.GetCurrentState(), blinkyState.shared.inactive);
  EXPECT_EQ(*rec, false);

}
