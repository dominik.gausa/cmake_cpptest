#include <gmock/gmock.h>
#include <iostream>

#include <Util/SmartPtr.h>

using namespace ::Platform::Util;

class TestObject
{
  public:
    void SayHi(){
      std::cout << "Hi!" << std::endl;
    }
};

void TestObject_caller_by_ptr(TestObject* tO)
{
  tO->SayHi();
}

void TestObject_caller_by_ref(TestObject& tO)
{
  tO.SayHi();
}

TEST(Platform_Util_SmartPtr, ThrowIfNull_normal_use)
{
  try{
    ThrowIfNull<TestObject> forcedNotToBeNull;
    forcedNotToBeNull = new TestObject;
    forcedNotToBeNull->SayHi();
    (*forcedNotToBeNull).SayHi();
    (&(*forcedNotToBeNull))->SayHi();

  }catch(::Platform::Exception& ex){
    std::cout << ex.what() << std::endl;
    FAIL() << "Shouldn't have thrown" << std::endl;
  }catch(...){
    FAIL() << "Unexpected Error" << std::endl;
  }
}

TEST(Platform_Util_SmartPtr, ThrowIfNull_use_uninitialized)
{
  try{
    ThrowIfNull<TestObject> throwIfNull;
    throwIfNull->SayHi();
    FAIL() << "Should have thrown" << std::endl;
  }catch(::Platform::Exception& ex){
    std::cout << ex.what() << std::endl;
  }catch(...){
    FAIL() << "Unexpected Error" << std::endl;
  }

  try{
    ThrowIfNull<TestObject> throwIfNull;
    TestObject_caller_by_ptr(throwIfNull);
    FAIL() << "Should have thrown" << std::endl;
  }catch(::Platform::Exception& ex){
    std::cout << ex.what() << std::endl;
  }catch(...){
    FAIL() << "Unexpected Error" << std::endl;
  }

  try{
    ThrowIfNull<TestObject> throwIfNull;
    (*throwIfNull).SayHi();
    TestObject_caller_by_ptr(throwIfNull);
    TestObject_caller_by_ref(*throwIfNull);
    FAIL() << "Should have thrown" << std::endl;
  }catch(::Platform::Exception& ex){
    std::cout << ex.what() << std::endl;
  }catch(...){
    FAIL() << "Unexpected Error" << std::endl;
  }
}
