
#include <gtest/gtest.h>
#include <Util/Error.h>


TEST(Exceptions, 1){
	try{
		DOEXCEPT("test")
		FAIL();
	}catch(Platform::Exception& ex){
		EXPECT_EQ(std::string(ex.what()).compare("test"), 0)
        << "ex.what is not what we've expected";
	}catch(...){
		FAIL() << "Exception is not an Platform::Exception";
	}
}

TEST(Exceptions, 2){
	try{
		DOEXCEPT("test2")
		FAIL();
	}catch(Platform::Exception& ex){
		EXPECT_EQ(std::string(ex.what()).compare("test2"), 0)
        << "ex.what is not what we've expected";
	}catch(...){
		FAIL() << "Exception is not an Platform::Exception";
	}
}

TEST(Exceptions, 3){	
	try{
		throw std::exception();
		FAIL();
	}catch(Platform::Exception& ex){
		FAIL() << "Exception should not be an Platform::Exception";
	}catch(...){
	}
}
