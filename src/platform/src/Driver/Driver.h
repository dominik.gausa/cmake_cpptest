#pragma once

#include <Util/NamedObject.h>

namespace Platform {
  namespace Driver {
    class Driver : public ::Platform::Util::NamedObject
    {
      public:
        Driver(const char* const name) :
          NamedObject(name)
        {
        }
    };
  };
};