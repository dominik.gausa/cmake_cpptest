#pragma once

#include <Driver/Driver.h>
#include <Util/Error.h>

namespace Platform {
  namespace Driver {
    class UART : public Driver {
      
      public:

        UART(const char * const name, unsigned int id):
          Driver(name),
          m_id(id)
        {}

        virtual bool SetBaudrate(unsigned int freq) = 0;

        virtual bool Send(const char *txt, unsigned int len)
        {
          for(unsigned int idx = 0; idx < len; idx++){
            Send(txt[idx]);
          }
        }

        virtual bool Send(char c){
          DOEXCEPT("not implemented")
        };

        virtual char ReadOne(){
          DOEXCEPT("not implemented")
          return 'X';
        };

      protected:
        unsigned int  m_id;
    };
  };
};