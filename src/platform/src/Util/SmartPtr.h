#pragma once

#include <Util/Error.h>

namespace Platform
{
  namespace Util
  {
    template<typename T>
    class ThrowIfNull
    {
      public:
        ThrowIfNull():
          _ptr(nullptr)
        {}
        
        T* operator =(T* b)
        {
          _ptr = b;
          return _ptr;
        }
        
        operator T*(){
          if(_ptr == nullptr){
            DOEXCEPT("Trying to use nullptr");
          }
          return _ptr;
        }
        
        T& operator*(){
          if(_ptr == nullptr){
            DOEXCEPT("Trying to use nullptr");
          }
          return *_ptr;
        }
        
        T* operator->(){
          if(_ptr == nullptr){
            DOEXCEPT("Trying to use nullptr");
          }
          return _ptr;
        }
        
        T* Get(){
          return _ptr;
        }
      
      private:
        T* _ptr;
    };

  }
}
