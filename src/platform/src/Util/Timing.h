#pragma once

#include <Util/Time.h>

namespace Platform
{
  namespace Timing
  {
    class Timer
    {
      static const unsigned long NOT_STARTED = -1;
      unsigned long m_start;
      unsigned long m_expire;

      public:
        Timer():
          m_start(-1),
          m_expire(-1)
        {}

        bool Started(){
          return m_start != NOT_STARTED;
        }

        void Stop(){
          m_start = NOT_STARTED;
        }

        bool Expired(){
          if(!Started()) return false;

          unsigned long now = Platform::Util::Time::Instance()->Uptime_ms();
          return now >= m_expire;
        }

        void Start(unsigned long timeout){
          m_start = Platform::Util::Time::Instance()->Uptime_ms();
          m_expire = m_start + timeout;
        }
    };
  }
}