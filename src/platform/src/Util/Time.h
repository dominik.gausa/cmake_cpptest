#pragma once

#include <Util/Singleton.h>
#include <Util/List.h>

namespace Platform 
{
  namespace Util
  {
    class TimeBase
    {
      public:
        typedef void (*SysTickHook)(void);

        TimeBase();
        virtual ~TimeBase();
        
        virtual void Start(float freq) final {
          m_tickFreq = freq;
          OnStart(freq);
        };

        /**
         * Get Uptime in Seconds
         */
        virtual float Uptime();

        /**
         * Get Uptime in Milliseconds
         */
        virtual unsigned long Uptime_ms();

        virtual void Tick() final {
          OnTick();
        };

        void AddHook(SysTickHook hook);

      protected:
        virtual void OnStart(float freq) = 0;
        virtual void OnTick();

        unsigned long       m_ticks;
        float               m_tickFreq;
        List<SysTickHook>   m_hooks;
    };

    class Time : public SingletonExtInit<TimeBase>
    {};

  }
}