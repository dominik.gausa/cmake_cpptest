#pragma once

#include <Util/Error.h>

namespace Platform
{
  namespace Util
  {
    class PreventDelete
    {

      public:
        PreventDelete(){}
        virtual ~PreventDelete(){
          DOEXCEPT("This Object must not be deleted");
        }
    };
  }
}