#pragma once

namespace Platform
{
  namespace Util
  {
    template<typename T, size_t size = 128>
    class SmartArrayBuffer
    {
      T m_data[size];

      unsigned int m_position;

      public:
        SmartArrayBuffer():
          m_position(0)
        {
        }

        void Reset()
        {
          m_position = 0;
        }

        bool Insert(T& value)
        {
          bool result = false;
          if(!IsFull())
          {
            m_data[m_position] = value;
            m_position++;
          }
          return result;
        }

        bool IsFull(){
          return m_position >= size;
        }

        T* GetData(){
          return m_data;
        }

        unsigned int GetDataCount()
        {
          return m_position;
        }
    };
  }
}
