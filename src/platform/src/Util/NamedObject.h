#pragma once

#include <string>

namespace Platform
{
  namespace Util
  {
    class NamedObject
    {
      const std::string m_name;

      public:
        NamedObject(const std::string& name) :
          m_name(name)
        {}

        const std::string& GetName(){
          return m_name;
        }
    };
  }
}