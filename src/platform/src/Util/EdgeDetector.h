#include <Util/EmitterReceiver.h>

namespace Platform
{
  namespace Util
  {
    class EdgeDetector : public DataEmitterReceiver<bool>
    {
      public:
        EdgeDetector(bool initial = false, bool transitionTo = true) :
          m_lastState(initial),
          m_transitionTo(transitionTo)
        {}

      protected:
        virtual bool OnReceive(const bool& data){
          bool result = true;
          
          result = Emit(m_lastState != data && m_transitionTo == data);
          
          return result;
        }

      private:
        bool      m_lastState;
        bool      m_transitionTo;
    };
  }
}
