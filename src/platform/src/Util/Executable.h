#pragma once


namespace Platform
{
  namespace Util
  {
    class Executable
    {
      public:
        virtual void Execute() final {
          OnExecute();
        }

      protected:
        virtual void OnExecute() = 0;
    };
  }
}