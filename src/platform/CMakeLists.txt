

############################################
## lib Math

set(libname platform)

file(GLOB_RECURSE SOURCES_${libname}_ASM "${CMAKE_CURRENT_LIST_DIR}/src/*.s")
file(GLOB_RECURSE SOURCES_${libname}_H "${CMAKE_CURRENT_LIST_DIR}/src/*.h")
file(GLOB_RECURSE SOURCES_${libname}_C "${CMAKE_CURRENT_LIST_DIR}/src/*.c")
file(GLOB_RECURSE SOURCES_${libname}_CXX "${CMAKE_CURRENT_LIST_DIR}/src/*.cpp")


set(SOURCES_${libname}
    ${SOURCES_${libname}_ASM}
    ${SOURCES_${libname}_H}
    ${SOURCES_${libname}_C}
    ${SOURCES_${libname}_CXX})

source_group(
    TREE ${CMAKE_CURRENT_LIST_DIR} 
    FILES ${SOURCES_${libname}}
    )

project(${libname})
add_library(${libname} STATIC ${SOURCES_${libname}})

target_include_directories(${libname} PUBLIC "${CMAKE_CURRENT_LIST_DIR}/src")
