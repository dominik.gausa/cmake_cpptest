#pragma once

#include <Driver/UART.h>
#include <Util/Error.h>

namespace Platform {
  namespace Driver {
    class UART_STM32F4 : public UART {
      
      public:

        UART_STM32F4(const char * const name, unsigned int id):
          UART(name, id)
        {
          PortEnable(m_id);
        }

        virtual bool SetBaudrate(unsigned int freq){
          DOEXCEPT("not implemented");
        }

        virtual bool Send(const char *txt, unsigned int len)
        {
          for(unsigned int idx = 0; idx < len; idx++){
            Send(txt[idx]);
          }
        }

        virtual bool Send(char c);

        virtual char ReadOne(){
          DOEXCEPT("not implemented")
          return 'X';
        };

      protected:
        virtual void PortEnable(unsigned int id);
    };
  };
};