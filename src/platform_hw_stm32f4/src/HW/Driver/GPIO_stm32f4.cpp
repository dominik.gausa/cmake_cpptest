#include "GPIO_stm32f4.h"
#include "stm32f4xx.h"

namespace Platform {
  namespace Driver {
    
    static GPIO_TypeDef* GetPort(unsigned int id){
      GPIO_TypeDef* ports[] = {
        GPIOA,
        GPIOB,
        GPIOC,
        GPIOD,
        GPIOE,
        GPIOF,
        GPIOG,
        GPIOH,
        GPIOI,
      };

      if(id > sizeof(ports)/sizeof(ports[0])){
        DOEXCEPT("Port does not exist");
      }
      return ports[id];
    }

    bool GPIO_STM32F4::SetMode(Mode mode){
      unsigned int tmp_moder = GetPort(m_pin.port)->MODER;
      tmp_moder &= ~(0x3<<(2*m_pin.pin));
      switch(mode){
        case INPUT:
          break;
        case OUTPUT:
            tmp_moder |= 0x1<<(2*m_pin.pin);
          break;
        case ANALOG:
            tmp_moder |= 0x3<<(2*m_pin.pin);
          break;
        case ALT_FUNC:
            tmp_moder |= 0x2<<(2*m_pin.pin);
          break;
      }
      GetPort(m_pin.port)->MODER = tmp_moder;
    }

    bool GPIO_STM32F4::Value(){
      return (GetPort(m_pin.port)->IDR & (1<<m_pin.pin)) ? true : false;
    }
    
    void GPIO_STM32F4::Set(const bool state){
      if(state){
        GetPort(m_pin.port)->ODR |= (1<<m_pin.pin);
      }else{
        GetPort(m_pin.port)->ODR &= ~(1<<m_pin.pin);
      }
    }

    void GPIO_STM32F4::PortEnable(unsigned int id){
      switch(id){
        case 0:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
          break;
        case 1:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
          break;
        case 2:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
          break;
        case 3:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
          break;
        case 4:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
          break;
        case 5:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
          break;
        case 6:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
          break;
        case 7:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
          break;
        case 8:
          RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
          break;
      }
    }
  }
}
