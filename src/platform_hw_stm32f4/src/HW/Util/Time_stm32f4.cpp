#include <Util/Time.h>
#include "Time_stm32f4.h"
#include <stm32f4xx.h>

namespace Platform 
{
  namespace Util
  {
    void Time_STM32F4::OnStart(float freq){
      SysTick_Config(static_cast<unsigned int>(SystemCoreClock/freq));
    }
  }
}

extern "C" void SysTick_Handler(){
  Platform::Util::Time::Instance()->Tick();
}
