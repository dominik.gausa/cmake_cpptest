	
#include <gmock/gmock.h>
#include <fstream>
#include <iostream>

struct DummyDataRow {
  double line, num;

  friend std::istream & operator >>(std::istream & in, DummyDataRow& data) {
    in >> data.line >> data.num;
    return in;
  }
};

TEST(platform_test_helper, read_DummyData) {
  std::fstream dummyData;
  dummyData.open(PATH_TO_TESTFILES "dummyFile.txt");
  if(dummyData.bad() || dummyData.fail() || dummyData.eof())
  {
    FAIL() << "Couldn't open File: " << PATH_TO_TESTFILES "dummyFile.txt";
  }

  int line_expect = 1;
  do{
    DummyDataRow data;
    dummyData >> data;
    if(dummyData.bad() || dummyData.fail() || dummyData.eof())
      break;

    EXPECT_EQ(line_expect, data.line);
    EXPECT_EQ(data.num, 123456789);

    line_expect++;
  }while(true);
  EXPECT_EQ(line_expect, 60);
  dummyData.close();
}
