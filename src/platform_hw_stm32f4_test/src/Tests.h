#pragma once

#include "cpptest.h"
#include <ostream>
#include <Util/Singleton.h>
#include <HW/Util/Trace.h>



class Tests : public Platform::Util::Singleton<Tests>
{
  public:
    Tests()
    { 
    }

    void Add(Test::Suite * suite){
      m_ts.add(std::auto_ptr<Test::Suite>(suite));
    }

    bool Run(){
      Test::Output* output = new Test::TextOutput(
                                  Test::TextOutput::Verbose,
                                  Platform::Semihost::Out());
      return m_ts.run(*output, true) ? 0 : -1;
    }

  private:  
	  Test::Suite m_ts;
};

template<typename T>
class TestAdder
{
  public:
    TestAdder(){
      Tests::Instance()->Add(new T);
    }
};
