#include <iostream>
#include <sstream>
#include <iomanip>

#include "Tests.h"
#include <Util/Error.h>
#include <Util/Time.h>
#include <Util/Timing.h>
#include <HW/Util/Trace.h>


class TEST_Semihosting : public Test::Suite
{
public:
	TEST_Semihosting()
	{
    TEST_ADD(TEST_Semihosting::ReadAndParse)
	}
	
private:
	void ReadAndParse() {
    try{
      Platform::Semihost::File file(PATH_TO_TESTFILES "dummyFile.txt", Platform::Semihost::File::Mode_Read);
      if(!file.Open()){
        TEST_FAIL("Couldnt open File");
        return;
      }

      char buffer;
      int len = -1;
      std::stringstream ss;
      int line_expect = 1;
      do{
        len = file.Read(&buffer, 1);
        if(len <= 0) break;
        if(buffer == '\r'){}
        else if(buffer == '\n'){
          std::istringstream in(ss.str());
          int line, num;
          in >> line;
          in >> num;

          TEST_ASSERT_EQUALS(line_expect, line);
          TEST_ASSERT_EQUALS(num, 123456789);
          //Platform::Semihost::Out() << "Line in: " << ss.str() << std::endl;
          //Platform::Semihost::Out() << "Line Parse: " << std::setw(5) << line << " num: " << num << std::endl;
          
          ss.str("");
          ss.clear();
          line_expect++;
        }else{
          ss << buffer;
        }
      }while(true);
      TEST_ASSERT_EQUALS(line_expect, 60);
    }catch(Platform::Exception& ex){
      TEST_FAIL("Unexpected Exception")
    }catch(...){
      TEST_FAIL("Unexpected Exception")
    }
  }
};

static TestAdder<TEST_Semihosting> test_Semihosting;
