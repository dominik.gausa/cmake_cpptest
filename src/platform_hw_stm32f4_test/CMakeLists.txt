  
############################################
## Test Application

set(projname platform_hw_stm32f4_test)


file(GLOB_RECURSE SOURCES_${projname}_ASM "${CMAKE_CURRENT_LIST_DIR}/src/*.s")
file(GLOB_RECURSE SOURCES_${projname}_H "${CMAKE_CURRENT_LIST_DIR}/src/*.h")
file(GLOB_RECURSE SOURCES_${projname}_C "${CMAKE_CURRENT_LIST_DIR}/src/*.c")
file(GLOB_RECURSE SOURCES_${projname}_CXX "${CMAKE_CURRENT_LIST_DIR}/src/*.cpp")


set(SOURCES_${projname}
    ${SOURCES_${projname}_ASM}
    ${SOURCES_${projname}_H}
    ${SOURCES_${projname}_C}
    ${SOURCES_${projname}_CXX})

source_group(
    TREE ${CMAKE_CURRENT_LIST_DIR} 
    FILES ${SOURCES_${projname}}
    )

project(${projname})
add_executable(${projname} ${SOURCES_${projname}})

include_directories("${CMAKE_CURRENT_LIST_DIR}/inc")
target_include_directories(${projname} PRIVATE  "${CMAKE_CURRENT_LIST_DIR}/src")
target_link_libraries (${projname} platform platform_hw_stm32f4 cpptest --coverage)

if(MSVC OR CMAKE_HOST_WIN32)
    set_property(TARGET ${projname} APPEND PROPERTY COMPILE_DEFINITIONS "PATH_TO_TESTFILES=\"..\\\\..\\\\ext\\\\\"")
else()
    set_property(TARGET ${projname} APPEND PROPERTY COMPILE_DEFINITIONS "PATH_TO_TESTFILES=\"../../ext/\"")
endif()


add_custom_target(${projname}_qemu
DEPENDS 
    ${projname}
WORKING_DIRECTORY
    ${CMAKE_CURRENT_LIST_DIR}
COMMAND
    ${tools_QEMU}qemu-system-gnuarmeclipse 
    --verbose
    --verbose
    --board STM32F4-Discovery 
    --mcu STM32F407VG
    -serial null
    -monitor null
    -nographic
    --gdb tcp::1234
    -d unimp,guest_errors
    --image ${CMAKE_CURRENT_BINARY_DIR}/${projname}
    -semihosting
    --semihosting-config enable=on,target=native
    --semihosting-cmdline test 1 2 3)


add_custom_target(${projname}_hex
DEPENDS 
    ${projname}
COMMAND
    ${tools_GCC}arm-none-eabi-objcopy -O binary "${CMAKE_CURRENT_BINARY_DIR}/${projname}" "${CMAKE_CURRENT_BINARY_DIR}/${projname}.hex"
)

set(BURN_APP_ADDRESS 0x08000000)
add_custom_target(${projname}_deploy
DEPENDS 
    ${projname}_hex
COMMAND 
    ${tools_STM}ST-LINK_CLI.exe -c SWD UR -HardRst -Halt; 
    ${tools_STM}ST-LINK_CLI.exe -c SWD UR -HardRst -P "${CMAKE_CURRENT_BINARY_DIR}/${projname}.hex" ${BURN_APP_ADDRESS};
    ${tools_STM}ST-LINK_CLI.exe -c SWD UR -HardRst;
)