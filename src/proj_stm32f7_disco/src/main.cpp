#include <stdio.h>

#include <Util/EdgeDetector.h>
#include <Util/Time.h>
#include <Util/Timing.h>
#include <Util/List.h>

#include "STM32F7_Discovery.h"
#include <app/BlinkyState.h>
#include <app/BlinkyTask.h>
#include <app/BlinkyExec.h>
#include <OS/Util/Timer.h>

using namespace ::Platform::Driver;
using namespace ::Platform::Util;
using namespace ::Platform::Timing;

static HardwareSTM32F7Discovery hw;
HardwareSTM32F7Discovery* HARDWARE = &hw;







extern "C" void xPortSysTickHandler(void);
volatile uint32_t sysTick = 0;
void sysTickIncrease(){
  sysTick++;
  if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED) {
    xPortSysTickHandler();
  }
}











EdgeDetector edgeDetect;
BlinkyState blinky;

int main(int argc, char** argv){
  auto time = Time::Instance();
  time->Start(1000);

  DataEmitter<bool> ledEmitter;
  ledEmitter.Link(HARDWARE->ledGreen);
  
  HARDWARE->btnUser->Link(&edgeDetect);
  edgeDetect.Link(&blinky.shared.trigger);
  blinky.shared.output.Link(HARDWARE->ledBlue);

  Time::Instance()->AddHook(sysTickIncrease);


  const HeapRegion_t xHeapRegions[] =
  {
    { ( uint8_t * ) 0x20000000UL, 0xFFFF },
    { NULL, 0 } /* Terminates the array. */
  };

  /* Pass the array into vPortDefineHeapRegions(). */
  vPortDefineHeapRegions( xHeapRegions );
         

  ExecutableTimer* execTimer_1s = new ExecutableTimer("execTimer", 1);
  ExecutableTimer* execTimer_10ms = new ExecutableTimer("execTimer", 0.01f);

  //execTimer_10ms->Add(HARDWARE->btnUser);
  execTimer_10ms->Add(&blinky);

  BlinkyExec* blinkyExec = new BlinkyExec;
  blinkyExec->outputState.Link(HARDWARE->ledRed);
  execTimer_1s->Add(blinkyExec);

  BlinkyTask* blinkyTask = new BlinkyTask;
  blinkyTask->outputState.Link(HARDWARE->ledOrange);

  vTaskStartScheduler();

  ::Platform::Timing::Timer timer;
  for(;;){
    if(timer.Expired() || !timer.Started()){
      timer.Start(100);

      HARDWARE->ledGreen->Set(!HARDWARE->ledGreen->Value());
    }
  }
}
