#include <Util/NamedObject.h>
#include <Util/Executable.h>
#include <Util/List.h>
#include <FreeRTOS.h>
#include <timers.h>

namespace Platform
{
  namespace Util
  {
    class SysTimer : public NamedObject
    {
      float m_interval;
      TimerHandle_t m_handle;

      public:
        SysTimer(
            const std::string& name, 
            float interval, 
            bool autoreload = true, 
            float delay = 0);

        virtual void Callback() final {
          OnCallback();
        }

      protected:
        virtual void OnCallback(){};
    };


    class ExecutableTimer : public SysTimer
    {
      List<Executable*> m_executables;

      public:
        ExecutableTimer(const std::string& name, float interval, bool autoreload = true) :
            SysTimer(name, interval, autoreload) 
          {}

        void Add(Executable* exec){
          m_executables.Add(exec);
        }

      protected:
        virtual void OnCallback();
        
    };

  }
}

