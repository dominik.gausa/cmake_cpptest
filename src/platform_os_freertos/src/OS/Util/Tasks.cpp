#include "Tasks.h"


namespace Platform
{
  namespace Util
  {
    void SysTaskCallback(void* task){
      ((SysTask*)task)->Run();
    }

    SysTask::SysTask(
          const std::string& name, 
          unsigned int stacksize,
          Priority priority) :
      Task(name, stacksize, priority)
    {
      xTaskCreate(
            SysTaskCallback,
            "SysTask",
            stacksize,
            ( void * ) this,
            priority,
            &m_handle );
    }
  }
}
