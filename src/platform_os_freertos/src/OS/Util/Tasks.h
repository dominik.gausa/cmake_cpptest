#pragma once

#include <Util/Tasks.h>
#include <FreeRTOS.h>
#include <task.h>

namespace Platform
{
  namespace Util
  {

    class SysTask : public Task
    {
      TaskHandle_t m_handle;

      public:
        SysTask(
              const std::string& name,
              unsigned int stacksize = 512,
              Priority priority = IDLE);
    };
  }
}
