############################################
## lib FreeRTOS



set(libname FreeRTOS)

if(NOT DEFINED TARGET_ARCH)
    error()
endif()

if("${TARGET_ARCH}" STREQUAL "ARM")
  if("${TARGET_MACH}" STREQUAL "ARM_CORTEX_M7")
    set(DIRS_${libname}_PORTABLE "${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1")
  elseif("${TARGET_MACH}" STREQUAL "ARM_CORTEX_M4")
    set(DIRS_${libname}_PORTABLE "${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM4F")
  elseif("${TARGET_MACH}" STREQUAL "ARM_CORTEX_M3")
    set(DIRS_${libname}_PORTABLE "${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM3")
  else()
    error()
  endif()
endif()

file(GLOB_RECURSE SOURCES_${libname}_PORTABLE_ASM ${DIRS_${libname}_PORTABLE}/*.s)
file(GLOB_RECURSE SOURCES_${libname}_PORTABLE_H ${DIRS_${libname}_PORTABLE}/*.h)
file(GLOB_RECURSE SOURCES_${libname}_PORTABLE_C ${DIRS_${libname}_PORTABLE}/*.c)

set(SOURCES_${libname}_PORTABLE
  ${SOURCES_${libname}_PORTABLE_ASM}
  ${SOURCES_${libname}_PORTABLE_H}
  ${SOURCES_${libname}_PORTABLE_C}
)

set(SOURCES_${libname}
  ${SOURCES_${libname}_PORTABLE}
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/portable/MemMang/heap_5.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/croutine.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/event_groups.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/list.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/queue.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/stream_buffer.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/tasks.c
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/timers.c
)

set(INCLUDES_${libname}
  ${CMAKE_CURRENT_LIST_DIR}/FreeRTOS/Source/include
  ${DIRS_${libname}_PORTABLE}
)

add_library(${libname} STATIC ${SOURCES_${libname}})
target_include_directories(${libname} PUBLIC ${INCLUDES_${libname}})
#set_target_properties(${libname} PROPERTIES COMPILE_DEFINITIONS ${DEFINES_LIB_MATH})
  
